#include "Laboratorio.h"
#include <list>
#include <random>
#include <ctime>
#include <algorithm>
#include <numeric>
#include "ABB.h"
#include "AVL.h"  
using namespace std;

list<int>::iterator iterador;

double Laboratorio::experimentoLista(int n) {
  list <int> lista;

  for(int i = 0; i < n; i++) {
    lista.push_back(rand() % 100000);
  }

  clock_t tiempo = clock();

  for(int i : lista) {
    iterador = find(lista.begin(), lista.end(), i);
  }

  return double (clock() - tiempo) / 1000;
}

vector <double> Laboratorio::repetirExperimentoLista(int n) {
  vector <double> tiempos;
  double desviacion = 0;

  for(int i = 0; i < 30; i++) {
    tiempos.push_back(experimentoLista(n));
  }

  double promedio = accumulate(tiempos.begin(), tiempos.end(), 0) / 30;
  
  for(int i = 0; i < 30; i++) {
    desviacion += pow((tiempos[i] - promedio), 2);
  }

  return {promedio, sqrt(desviacion / 30)};
}

double Laboratorio::experimentoAVL(int n) {
  list <int> lista;

  for(int i = 0; i < n; i++) {
    lista.push_back(rand() % 100000);
  }
  
  AVL avl;

  for (int i : lista) {
    avl.insertar(i);
  }
  
  clock_t tiempo = clock();
  
  for(int i : lista) {
    if(avl.buscar(i) == 1) {
      continue;
    }
  }
  
  return double (clock() - tiempo) / 1000;
}

vector<double> Laboratorio::repetirExperimentoAVL(int n) {
  vector <double> tiempos;
  double desviacion = 0;
  
  for (int i = 0; i < 30; i++) {
    tiempos.push_back(experimentoAVL(n));
  }
  
  double promedio = accumulate(tiempos.begin(), tiempos.end(), 0) / 30;
  
  for (int i = 0; i < 30; i++) {
    desviacion += pow((tiempos[i] - promedio), 2);
  }
  
  return {promedio, sqrt(desviacion / 30)};
}

double Laboratorio::experimentoABB(int x){
  list <int> lista;

  for(int i = 0; i < x; i++) {
    lista.push_back(rand() % 100000);
  }

    ABB arbol = ABB();

    for (int i : lista) {

        arbol.Insertar(i);
    }

    clock_t tiempo = clock();

    for(int i : lista) {
    if(arbol.Buscar(i) == 1) {continue;}
    }

    return double (clock() - tiempo) / 1000;
}

vector<double> Laboratorio::repetirExperimentoABB(int x) {

    vector <double> tiempos;
    double desviacion = 0;

    for (int i = 0; i < 30; i++) {

        tiempos.push_back(experimentoABB(x));
    }
    
    double promedio = accumulate(tiempos.begin(), tiempos.end(), 0) / 30;

    for (int i = 0; i < 30; i++) {

        desviacion += pow((tiempos[i] - promedio), 2);
    }

    return { promedio,sqrt(desviacion / 30) };
}