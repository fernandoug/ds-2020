#include "Nodo.h"

class AVL {
  public:
  enum class Escenario {IzqIzq, DerDer, IzqDer, DerIzq, None};

  int altura(Nodo*);
  Escenario determinarEscenario(Nodo* nodo);
  void imprimir(Nodo*);

  Nodo* raiz = nullptr;
  int factorEquilibrio(Nodo* nodo);
  Nodo* rotIzqIzq(Nodo* padre);
  Nodo* rotDerDer(Nodo* padre);
  Nodo* rotIzqDer(Nodo* padre);
  Nodo* rotDerIzq(Nodo* padre);
  Nodo* balancear(Nodo*, int);
  void insertar(int);
  Nodo* insertar(Nodo*, int);
  bool buscar(int);

};