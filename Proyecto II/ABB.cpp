#include "ABB.h"
#include "NodoBB.h"
#include <iostream>

using namespace std;
ABB::ABB()
{
	raiz = nullptr;
}

bool ABB::Buscar(int x) {

	NodoBB* actual = raiz;

	while (actual != nullptr) {

		if (actual->valor == x) {

			return true;
		}

		else if (x < actual->valor) {

			actual = actual->izquierdo;

		}

		else {

			actual = actual->derecho;

		}
	}

	return false;
}

NodoBB* ABB::Insertar(NodoBB* nodo,int x) {

	if (nodo== nullptr) {

		NodoBB* nuevo = new NodoBB(x);

		return nuevo;
	}

	else if (nodo->valor > x) {

			nodo->izquierdo = Insertar(nodo->izquierdo, x);

			return nodo;

	}

	else {

			nodo->derecho = Insertar(nodo->derecho,x);

			return nodo;
	}

}

void ABB::Insertar(int x) {

	raiz = Insertar(raiz, x);
}

void ABB::imprimir(NodoBB* nodo) {

  if(nodo == nullptr) {

     return;
  }
  imprimir(nodo->izquierdo);
  cout << nodo->valor << ' ';
  imprimir(nodo->derecho);
}
