#include "AVL.h"
#include <iostream>
using namespace std;

void AVL::insertar(int n) {
  raiz = insertar(raiz, n);
  raiz = balancear(raiz, n);
}

Nodo* AVL::insertar(Nodo* nodo, int n) {
  if(nodo == nullptr) {
    Nodo* nuevoNodo = new Nodo(n);
    return nuevoNodo;
  }
  else if(nodo->valor > n) {
    nodo->izquierdo = insertar(nodo->izquierdo, n);
    return nodo;
  }
  else {
    nodo->derecho = insertar(nodo->derecho, n);
    return nodo;
  }

  nodo->altura = max(altura(nodo->izquierdo), altura(nodo->derecho)) + 1;
}

int AVL::altura(Nodo* nodo) {
  if(nodo == nullptr) {
		return 0;
	}
	return nodo->altura;
}

Nodo* AVL::balancear(Nodo* nodo, int n) {
  switch(determinarEscenario(nodo)) {
    case Escenario::IzqIzq:
    return rotIzqIzq(nodo);
    case Escenario::IzqDer:
    return rotIzqDer(nodo);
    case Escenario::DerDer:
    return rotDerDer(nodo);
    case Escenario::DerIzq:
    return rotDerIzq(nodo);
    case Escenario::None:
    return nodo;
  }
}

AVL::Escenario AVL::determinarEscenario(Nodo *nodo) {
  if(factorEquilibrio(nodo) > 1 && factorEquilibrio(nodo->izquierdo) > 0) {return Escenario::IzqIzq;}
  else if(factorEquilibrio(nodo) > 1 && factorEquilibrio(nodo->izquierdo) <= 0) {return Escenario::IzqDer;}
  else if(factorEquilibrio(nodo) < -1 && factorEquilibrio(nodo->derecho) < 0) {return Escenario::DerDer;}
  else if(factorEquilibrio(nodo) < -1 && factorEquilibrio(nodo->derecho) >= 0) {return Escenario::DerIzq;}
  else {return Escenario::None;}
}

int AVL::factorEquilibrio(Nodo* nodo) {
  return altura(nodo->izquierdo) - altura(nodo->derecho);
}

Nodo* AVL::rotIzqIzq(Nodo *padre) {
  Nodo* b = padre->izquierdo;
  Nodo* Br = b->derecho;
  padre->izquierdo = Br;
  b->derecho = padre;
  padre->altura = max(altura(padre->izquierdo), altura(padre->derecho)) + 1;
  b->altura = max(altura(b->izquierdo), altura(b->derecho)) + 1;
  return b;
}

Nodo* AVL::rotDerDer(Nodo *padre) {
  Nodo* b = padre->derecho;
  Nodo* Bl = b->izquierdo;
  padre->derecho = Bl;
  b->izquierdo = padre;
  padre->altura = max(altura(padre->izquierdo), altura(padre->derecho)) + 1;
  b->altura = max(altura(b->izquierdo), altura(b->derecho)) + 1;
  return b;
}

Nodo* AVL::rotIzqDer(Nodo *padre) {
  Nodo* b = padre->izquierdo;
  padre->derecho = rotDerDer(b);
  return rotIzqIzq(padre);
}

Nodo* AVL::rotDerIzq(Nodo *padre) {
  Nodo* b = padre->derecho;
  padre->izquierdo = rotIzqIzq(b);
  return rotDerDer(padre);
}

bool AVL::buscar(int n) {
  Nodo* indice = raiz;

  while(indice != nullptr) {
    if(indice->valor == n) {
      return true;
		}
    else if(n < indice->valor) {
      indice = indice->izquierdo;
    }
    else {
      indice = indice->derecho;
    }
	}
	return false;
}

void AVL::imprimir(Nodo* nodo) {
  if(nodo == nullptr) {
     return;
  }
  imprimir(nodo->izquierdo);
  cout << nodo->valor << ' ';
  imprimir(nodo->derecho);
}
