class Nodo {
  public:
  int valor, altura = 1;
  Nodo* izquierdo = nullptr;
  Nodo* derecho = nullptr;

  Nodo(int);
};