#include <vector>
using namespace std;

class Laboratorio {

  public:
  double experimentoLista(int);
  double experimentoABB(int);
  double experimentoAVL(int);
  vector <double> repetirExperimentoLista(int);
  vector <double> repetirExperimentoABB(int);
  vector <double> repetirExperimentoAVL(int);
  
};