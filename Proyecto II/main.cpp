#include <iostream>
#include "Laboratorio.h"
#include "ABB.h"
#include "AVL.h"
#include <iomanip>
using namespace std;

Laboratorio laboratorio;

int main() {
  cout << fixed << setprecision(8) << "(c) 2020 Laboratorios Mora Ugalde S.A. Todos los derechos reservados.";

  cout << "\n\nEXPERIMENTO 1 (10 números)";

  cout << "\n\nLista:";
  vector <double> res = laboratorio.repetirExperimentoLista(10);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nABB:";
  laboratorio.repetirExperimentoABB(10);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nAVL:";
  res = laboratorio.repetirExperimentoAVL(10);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nEXPERIMENTO 2 (1000 números)";

  cout << "\n\nLista:";
  laboratorio.repetirExperimentoLista(1000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nABB:";
  res = laboratorio.repetirExperimentoABB(1000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nAVL:";
  res = laboratorio.repetirExperimentoAVL(1000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];
  
  cout << "\n\nEXPERIMENTO 3 (100000 números)";

  cout << "\n\nABB:";
  res = laboratorio.repetirExperimentoABB(100000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nAVL:";
  res = laboratorio.repetirExperimentoAVL(100000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];

  cout << "\n\nLista:";
  laboratorio.repetirExperimentoLista(100000);
  cout << "\n\nPromedio: " << res[0];
  cout << "\nDesviación: " << res[1];
}