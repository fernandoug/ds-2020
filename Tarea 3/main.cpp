#include "Grafo.h"

int main() {
  Grafo* grafo = new Grafo(4);
  grafo->setArista(0, 1, 4);
  grafo->setArista(1, 2, 5);
  grafo->setArista(2, 3, 6);
  grafo->setArista(3, 0, 7);
  cout << grafo->toString() << '\n';
  cout << grafo->getArista(0, 1) << '\n';
  cout << grafo->getArista(1, 2) << '\n';
  cout << grafo->getArista(2, 3) << '\n';
  cout << grafo->getArista(3, 0);
  return 0;
}