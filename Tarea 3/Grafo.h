#pragma once

#include <map>
#include <vector>
#include "Nodo.h"
#include <iostream>
#include "Arista.h"
using namespace std;

class Grafo {
  public:
  map<vector<int>, int> listaAristas;
  int numeroVertices;
  Nodo** cabeza;

  Grafo(int);
  ~Grafo();
  int getArista(int, int);
  void setArista(int, int, int);
  string toString();
};