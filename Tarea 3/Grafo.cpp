#include "Grafo.h"

Grafo::Grafo(int nV) {
  numeroVertices = nV;

  for(int i; i < numeroVertices; i++) {
    cabeza[i] = nullptr;
  }
}

Grafo::~Grafo() {
  for(int i; i < numeroVertices; i++) {
    delete[] cabeza[i];
  }
  delete[] cabeza;
}

int Grafo::getArista(int i, int j) {
  return listaAristas[{i, j}];
}

void Grafo::setArista(int i, int j, int peso) {
  Arista arista;
  arista.vertice1 = i;
  arista.vertice2 = j;
  arista.peso = peso;
  listaAristas[{i, j}] = peso;

  Nodo* nodo;
  nodo->valor = arista.vertice2;
  nodo->costo = arista.peso;
  nodo->siguiente = cabeza[arista.vertice1];

  cabeza[arista.vertice1] = nodo;
}

string Grafo::toString() {
  string res;

  for(int i; i < numeroVertices; i++) {
    Nodo* indice = cabeza[i];

    while(indice != nullptr) {
      res += "(Vértice 1: " + to_string(i) + ", Vértice 2: " + to_string(indice->valor) + ", Peso: " + to_string(indice->costo) + ')';
      indice = indice->siguiente;
    }
    res += '\n';
  }
  return res;
}