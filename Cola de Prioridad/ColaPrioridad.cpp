#include "ColaPrioridad.h"
#include "Pasajero.h"
#include "Nodo.h"
#include <iostream>
using namespace std;

bool ColaPrioridad::colaVacia() {return cabeza == nullptr;}

void ColaPrioridad::insertarOrdenado(Pasajero pasajero) {
    Nodo* nodo = new Nodo(pasajero, nullptr);

    if(colaVacia()) {cabeza = nodo;}
    else if(nodo->pasajero.getPrioridad() < cabeza->pasajero.getPrioridad()) {
      nodo->siguiente = cabeza;
      cabeza = nodo;
    }
    else {
      Nodo* indice = cabeza;
      while(indice->siguiente != nullptr && indice->pasajero.getPrioridad() < nodo->pasajero.getPrioridad()) {indice = indice->siguiente;}
      nodo->siguiente = indice->siguiente;
      indice->siguiente = nodo;
    }
  }

void ColaPrioridad::eliminarFinal() {
    if(cabeza->siguiente == nullptr) {
      delete cabeza;
      cabeza = nullptr;
    }
    else {
      Nodo* indice = cabeza;
      while(indice->siguiente->siguiente != nullptr) {indice = indice->siguiente;}
      delete indice->siguiente;
      indice->siguiente = nullptr;
    }
  }

void ColaPrioridad::imprimir() {
    Nodo* indice = cabeza;

    while(indice->siguiente != nullptr) {
      indice->pasajero.imprimir(); cout << " > ";
      indice = indice->siguiente;
    }
    indice->pasajero.imprimir();
  }