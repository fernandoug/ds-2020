#pragma once
#include "Pasajero.h"

class Nodo {
  public:
  Pasajero pasajero;
  Nodo* siguiente;
  Nodo(Pasajero p, Nodo* s);
};