#pragma once
#include "Nodo.h"
#include "Pasajero.h"

class ColaPrioridad {
  public:
  Nodo* cabeza = nullptr;
  bool colaVacia();
  void insertarOrdenado(Pasajero);
  void eliminarFinal();
  void imprimir();
};