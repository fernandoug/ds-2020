#include "Pasajero.h"
#include <iostream>
using namespace std;

int Pasajero::getPrioridad() {
    if((clasePasajero == 0 && sexo == 'M') || edad < 13) {return 0;}
    else if((clasePasajero == 1 && sexo == 'M') || edad < 13) {return 1;}
    return 2;
  }

void Pasajero::imprimir() {cout << clasePasajero << '-' << edad << '-' << sexo << '-' << getPrioridad();}