#include <vector>
#include "ColaPrioridad.h"
#include <iostream>
#include "Pasajero.h"
using namespace std;

int main() {
  vector <char> sexos = {'H', 'M'};
  ColaPrioridad colaPasajeros;

  cout << "El barco está compuesto por hombres y mujeres de clase 0, 1 y 2, con rango de edad de 0 a 26 años.\n\nSintaxis: clasePasajero-edad-sexo-prioridad\n\n";

  for(int i; i < 10; i++) {
    Pasajero pasajero;
    pasajero.clasePasajero = rand() % 3;
    pasajero.edad = rand() % 27;
    pasajero.sexo = sexos[rand() % 2];
    colaPasajeros.insertarOrdenado(pasajero);
    
    cout << "Pasajero " << i + 1 << ": "; pasajero.imprimir(); cout << '\n';
  }

  cout << "\nCola: "; colaPasajeros.imprimir();

  for(int i; i < 5; i++) {
    colaPasajeros.eliminarFinal();
  }
  cout << "\n\nEliminamos a los últimos 5...\n\nSobrevivientes: "; colaPasajeros.imprimir();
}