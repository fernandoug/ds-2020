#pragma once
#include "Nodo.h"
#include "Bigrama.h"
#include <iostream>
#include "ColaBigramas.h"
#include <sstream>
#include <fstream>
using namespace std;

class ColaBigramas {
  public:
  Nodo* cabeza = nullptr;
  Nodo* fin = nullptr;
  
  void encolar(Bigrama bigrama);
  void desencolar();
  string toString();
  ColaBigramas* stringACola(string texto);
  ColaBigramas* archivoACola(string archivo);
};