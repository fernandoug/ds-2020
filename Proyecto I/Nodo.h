#pragma once
#include "Bigrama.h"

class Nodo {
  public:
  Bigrama bigrama;
  Nodo* siguiente;

  Nodo(Bigrama b, Nodo* s);
};