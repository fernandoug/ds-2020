#include <iostream>
#include "Bigrama.h"
#include "ColaBigramas.h"
#include "AutoCompletador.h"
#include "ListaCuentaPalabras.h"
using namespace std;

int main() {
  /*
  while(true) {
    int opcion, numero;
    string texto;
    ColaBigramas* cola{};
    AutoCompletador completador;
    Bigrama bigrama;

    cout << "\n1. Cargar string\n" <<
    "2. Cargar texto\n" <<
    "3. Autocompletar\n" <<
    "4. Salir\n" <<
    "Digite un número y presione Enter: "; cin >> opcion; cout << '\n';

    switch(opcion) {
      case 1:
      cout << "Texto: "; cin >> texto;
      completador.procesarColaBigramas(*cola->stringACola(texto));
      break;

      case 2:
      cout << "Archivo: "; cin >> texto;
      cola->archivoACola(texto);
      completador.agregarBigrama(bigrama);
      break;

      case 3:
      cout << "Texto: "; cin >> texto;
      cout << "\nNúmero: "; cin >> numero;
      completador.predecir(texto, numero);
    }
  }
  */
  Bigrama bigrama1, bigrama2, bigrama3;

    bigrama1.primeraPalabra = "Hola";
    bigrama1.segundaPalabra = "Mundo";
    cout << bigrama1.imprimir() + '\n';

    bigrama2.primeraPalabra = "Hola";
    bigrama2.segundaPalabra = "Perro";
    cout << bigrama2.imprimir() + '\n';

    bigrama3.primeraPalabra = "Hola";
    bigrama3.segundaPalabra = "Mundo";
    cout << bigrama3.imprimir() + '\n';

    ColaBigramas cola;
    cola.encolar(bigrama1);
    cola.encolar(bigrama2);
    cola.encolar(bigrama3);
    cout << cola.toString() + '\n';
    cola.desencolar();
    cout << cola.toString();
}