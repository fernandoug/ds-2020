#pragma once
#include "CuentaPalabra.h"

class NodoPalabra {
  public:
  CuentaPalabra cuentaPalabra;
  NodoPalabra* siguiente;

  NodoPalabra(CuentaPalabra c, NodoPalabra* s);
};