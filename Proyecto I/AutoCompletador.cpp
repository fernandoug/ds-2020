#include "AutoCompletador.h"

void AutoCompletador::agregarBigrama(Bigrama bigrama) {
  DiccionarioCuentas diccionario;
  ListaCuentaPalabras* lista = diccionario.buscar(bigrama.primeraPalabra);

  if(lista != nullptr) {
    lista->aumentarPalabra(bigrama.segundaPalabra);
  }
  else {
    ListaCuentaPalabras lista2;
    lista2.primeraPalabra = bigrama.primeraPalabra;
    lista2.aumentarPalabra(bigrama.segundaPalabra);
    diccionario.insertar(lista2);
  }
}

void AutoCompletador::procesarColaBigramas(ColaBigramas cola) {
  while(cola.cabeza != nullptr) {
    agregarBigrama(cola.cabeza->bigrama);
    cola.desencolar();
  }
}

ListaCuentaPalabras* AutoCompletador::obtenerCuentaPalabras(string primeraPalabra) {
  DiccionarioCuentas diccionario;
	ListaCuentaPalabras* lista = diccionario.buscar(primeraPalabra);
	lista->ordenar();
	return lista;
}

string AutoCompletador::predecir(string primeraPalabra, int n) {
  int indice = n;
	ListaCuentaPalabras* lista = obtenerCuentaPalabras(primeraPalabra);

	while (lista != nullptr && indice < n) {
    lista->primeraPalabra = primeraPalabra;
		indice++;
	}
	
  return primeraPalabra + ": " + to_string(n);
}