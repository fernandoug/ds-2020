#pragma once
#include <string>
#include "NodoPalabra.h"
#include "Bigrama.h"
using namespace std;

class ListaCuentaPalabras {
  public:
	NodoPalabra* cabeza = nullptr;
  bool ordenado = false;
	string primeraPalabra;

	void aumentarPalabra(string SegundaPalabra);
	void ordenar();
	void MergeSort(NodoPalabra* referenciaCabeza);
	void DividirLista(NodoPalabra* origen, NodoPalabra* izq, NodoPalabra* der);
	NodoPalabra* Merge(NodoPalabra* izq, NodoPalabra* der);
};