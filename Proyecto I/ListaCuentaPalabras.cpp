#include "ListaCuentaPalabras.h"

void ListaCuentaPalabras::aumentarPalabra(string SegundaPalabra) {
  if(cabeza == nullptr) {return;}

	NodoPalabra* indice = cabeza;
	CuentaPalabra palabra;
	while (indice != nullptr) {
		if (SegundaPalabra == indice->cuentaPalabra.palabra)
			aumentarPalabra(SegundaPalabra);
			palabra.aumentar();
	}

	CuentaPalabra cuentaPalabra;
	NodoPalabra* siguiente = nullptr;
	cuentaPalabra.palabra = SegundaPalabra;
	NodoPalabra* nuevoNodo = new NodoPalabra(cuentaPalabra, siguiente);

	if (cabeza == nullptr) {
		cabeza = nuevoNodo;
		palabra.aumentar();
	}
	else if (SegundaPalabra <= cabeza->cuentaPalabra.palabra) {
		nuevoNodo->siguiente = cabeza;
		cabeza = nuevoNodo;
		palabra.aumentar();
	}
	else {
		NodoPalabra* nodoAnterior = cabeza;
		NodoPalabra* nodoSiguiente = cabeza->siguiente;
		while (nodoSiguiente != nullptr && nodoSiguiente->cuentaPalabra.palabra < SegundaPalabra) {
			nodoAnterior = nodoSiguiente;
			nodoSiguiente = nodoSiguiente->siguiente;
			aumentarPalabra(SegundaPalabra);
			palabra.aumentar();
		}
		nodoAnterior->siguiente = nuevoNodo;
		nuevoNodo->siguiente = nodoSiguiente;
		palabra.aumentar();
	}
  ordenado = false;
}

void ListaCuentaPalabras::ordenar() {
	NodoPalabra* lista = nullptr;
	
	if (lista != nullptr) {
		MergeSort(lista);
	}

	ordenado = true;
}

void ListaCuentaPalabras::MergeSort(NodoPalabra* referenciacabeza) {
	NodoPalabra* cabeza = referenciacabeza;
	NodoPalabra* izq1 = nullptr;
	NodoPalabra* der1 = nullptr;

	if (cabeza == nullptr || cabeza->siguiente == nullptr) {return;}

	DividirLista(cabeza, izq1, der1);
	MergeSort(izq1);
	MergeSort(der1);
	referenciacabeza = Merge(izq1, der1);
}

void ListaCuentaPalabras::DividirLista(NodoPalabra* origen, NodoPalabra* izq, NodoPalabra* der) {
	NodoPalabra* lento = origen;
	NodoPalabra* rapido = origen->siguiente;

	while (rapido != nullptr) {
		rapido = rapido->siguiente;
		if (rapido != nullptr) {
			lento = lento->siguiente;
			rapido = rapido->siguiente;
		}
	}

	izq = origen;
	der = lento->siguiente;
	lento->siguiente = nullptr;
}

NodoPalabra* ListaCuentaPalabras::Merge(NodoPalabra* izq, NodoPalabra* der) {
  NodoPalabra* nuevacabeza = nullptr;

	if (izq == nullptr) {return der;}
	if (der == nullptr) {return izq;}

	if (izq->cuentaPalabra.palabra >= der->cuentaPalabra.palabra) {
		nuevacabeza = izq;
		nuevacabeza->siguiente = Merge(izq->siguiente, der);
	}
	else {
		nuevacabeza = der;
		nuevacabeza->siguiente = Merge(izq, der->siguiente);
	}

	return nuevacabeza;
}