#pragma once
#include <map>
#include "ListaCuentaPalabras.h"
#include <iostream>
using namespace std;

class DiccionarioCuentas {
  public:
  map<string, ListaCuentaPalabras> diccionario;
  void insertar(ListaCuentaPalabras listaCuentas);
  ListaCuentaPalabras* buscar(string primeraPalabra);
};