#include "DiccionarioCuentas.h"

void DiccionarioCuentas::insertar(ListaCuentaPalabras listaCuentas) {
  diccionario[listaCuentas.primeraPalabra] = listaCuentas;
}

ListaCuentaPalabras* DiccionarioCuentas::buscar(string primeraPalabra) {
  return &diccionario[primeraPalabra];
}