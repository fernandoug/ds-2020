#pragma once
#include "Bigrama.h"
#include "ColaBigramas.h"
#include "ListaCuentaPalabras.h"
#include <iostream>
#include "DiccionarioCuentas.h"
using namespace std;

class AutoCompletador {
  public:
  void agregarBigrama(Bigrama bigrama);
  void procesarColaBigramas(ColaBigramas cola);
  ListaCuentaPalabras* obtenerCuentaPalabras(string primeraPalabra);
  string predecir(string primeraPalabra, int n);
};