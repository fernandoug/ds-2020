#include "ColaBigramas.h"
#include "Bigrama.h"
#include "Nodo.h"

void ColaBigramas::encolar(Bigrama bigrama) {
  Nodo* nodo = new Nodo(bigrama, nullptr);

  if (cabeza == nullptr) {
    nodo->siguiente = cabeza;
    cabeza = nodo;
    fin = cabeza;
  }

  fin->siguiente = nodo;
  fin = nodo;
}

void ColaBigramas::desencolar() {
  if(cabeza == nullptr) {return;}
  Nodo* tmp = cabeza;
  cabeza = cabeza->siguiente;
  delete tmp;
}

string ColaBigramas::toString() {
  string res = "[";
  Nodo* indice = cabeza;

  while(indice->siguiente != nullptr) {
    res += indice->bigrama.primeraPalabra + "--" + indice->bigrama.segundaPalabra + ", ";
    indice = indice->siguiente;
  }

  return res + indice->bigrama.primeraPalabra + "--" + indice->bigrama.segundaPalabra + "]";
}

ColaBigramas* ColaBigramas::stringACola(string texto) {
  stringstream stexto(texto);
  string palabra1, palabra2;
  getline(stexto, palabra1, ' ');
  ColaBigramas cola;

  while(getline(stexto, palabra2, ' ')) {
    Bigrama* bigrama;
    bigrama->primeraPalabra = palabra1;
    bigrama->segundaPalabra = palabra2;
    cola.encolar(*bigrama);
    palabra1 = palabra2;
  }

  return &cola;
}

ColaBigramas* ColaBigramas::archivoACola(string archivo) {
  ifstream admArchivo;
  string texto = " ";
  admArchivo.open(archivo, std::ios::in);
  getline(admArchivo, texto);
  admArchivo.close();
  return stringACola(texto);
}