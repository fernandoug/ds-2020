class Alumno {
  private:
    std::string nombre;
    std::string apellido;

  public:
    Alumno(std::string, std::string);
    std::string getNombre();
    void setNombre(std::string);
    std::string getApellido();
    void setApellido(std::string);
};

class Nodo {
  public:
    Nodo();
	  Nodo(Alumno&);
    Nodo* siguiente;
    Alumno valor;
};

class PilaDeAlumnos {
  Nodo* cabeza;
  bool pilaVacia();
  void push(Alumno);
  Alumno peek();
  Alumno pop();
};