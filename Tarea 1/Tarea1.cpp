#include <iostream>

class Alumno {
  private:
    std::string nombre;
    std::string apellido;

  public:
    Alumno(std::string nuevoNombre, std::string nuevoApellido) {
      nombre = nuevoNombre;
      apellido = nuevoApellido;
    }

    std::string getNombre() {
      return nombre;
    }

    void setNombre(std::string nuevoNombre) {
      nombre = nuevoNombre;
    }

    std::string getApellido() {
      return apellido;
    }

    void setApellido(std::string nuevoApellido) {
      apellido = nuevoApellido;
    }
};

class Nodo {
  public:
    Nodo();
	  Nodo(Alumno& alumno);
    Nodo* siguiente;
    Alumno valor;
};

class PilaDeAlumnos {
  Nodo* cabeza;

  bool pilaVacia() {
    if(cabeza == nullptr) {
      return true;
    }
    return false;
  }

  void push(Alumno alumno) {
    Nodo* nuevoNodo = new Nodo(alumno);
    if(pilaVacia()) {
      cabeza = nuevoNodo;
    } else {
      nuevoNodo->siguiente = cabeza;
      cabeza = nuevoNodo;
    }
  }

  Alumno peek() {
    return cabeza->valor;
  }
  
  Alumno pop() {
    Alumno tmp = cabeza->valor;
    cabeza = cabeza->siguiente;
    return tmp;
  }
};

int main() {

}