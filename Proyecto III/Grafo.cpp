#include "Grafo.h"

Grafo::Grafo()
{
	Vertices.reserve(500);
	NumeroVertices = 1;
}

Grafo::~Grafo()
{
}

void Grafo::AgregarVertice(int id, std::string nombre, double latitud, double longitud)
{
	Vertices.emplace_back(id, nombre, latitud, longitud);
	NumeroVertices++;
}

void Grafo::AgregarArista(int estacion1, int estacion2, int tiempo)
{
	Vertices[estacion1].Vecinos.emplace_back(estacion2, tiempo);

	Vertices[estacion2].Vecinos.emplace_back(estacion1, tiempo);


}

int Grafo::EncontrarVerticePorNombre(std::string nombre)
{
	for (Vertice v : Vertices) {

		if (v.Nombre == nombre) {

			return v.Id;
		}
	}
	//TODO: deben completar este m�todo, a partir del nombre devolver su id.
	return -1;
}
