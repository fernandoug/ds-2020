#include "Dijkstra.h"
#include <iostream>


using namespace std;

void Dijkstra::Inicializar(Grafo& grafo, int fuente) {
    for (auto& v : grafo.Vertices) {
        if (v.Id == fuente) {
            Tabla.emplace_back(false, 0, TablaGrafos::ValorVacio());
        }
        else {
            Tabla.emplace_back(false, TablaGrafos::ValorDistanciaInfinita(), TablaGrafos::ValorVacio());
        }
    }
}

void Dijkstra::Ejecutar(Grafo& grafo, int fuente) {
    Inicializar(grafo, fuente);
    int minimo = EncontrarMinimioSinVisitar();

    while (minimo != -1) {
        Tabla[minimo].Visitado = true;
        list<Arista> vecinos = grafo.Vertices[minimo].Vecinos;

        for (auto& i : vecinos) {
            if (Tabla[i.VerticeAdyacente].Visitado == false && Tabla[minimo].Distancia + i.Tiempo < Tabla[i.VerticeAdyacente].Distancia) {
                Tabla[i.VerticeAdyacente].Distancia = Tabla[minimo].Distancia + i.Tiempo;
                Tabla[i.VerticeAdyacente].Anterior = minimo;
            }
        }
    }
}

int Dijkstra::EncontrarMinimioSinVisitar() {

    int distanciaMinima = TablaGrafos::ValorDistanciaInfinita();

    int indice;

    for (int i = 0; i < Tabla.size(); i++) {
        if (Tabla[i].Visitado == false && Tabla[i].Distancia < distanciaMinima) {
            distanciaMinima = Tabla[i].Distancia;
        }
         indice = i;
        
       
    }
    return indice;
}

string Dijkstra::RutaMasCortaA(Grafo& grafo, int destino) {
    string res;

    while (Tabla[destino].Distancia != 0) {
        res = "->" + grafo.Vertices[destino].Nombre + res;
        destino = Tabla[destino].Anterior;
    }
    res = grafo.Vertices[destino].Nombre + res;
    return res;
}