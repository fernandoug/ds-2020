#include "AEstrella.h"
#include <math.h>
#include <iostream>

using namespace std;

void AEstrella::Inicializar(Grafo& grafo, int fuente) {
    for (auto& v : grafo.Vertices) {
        if (v.Id == fuente) {
            Tabla.emplace_back(false, 0, TablaGrafos::ValorVacio());
        }
        else {
            Tabla.emplace_back(false, TablaGrafos::ValorDistanciaInfinita(), TablaGrafos::ValorVacio());
        }
    }
}

void AEstrella::Ejecutar(Grafo& grafo, int fuente) {
    Inicializar(grafo, fuente);
    int minimo = EncontrarMinimioSinVisitar();
    int lat1 = grafo.Vertices[fuente].Latitud;
    int lon1 = grafo.Vertices[fuente].Longitud;

    while (minimo != -1) {
        Tabla[minimo].Visitado = true;
        list<Arista> vecinos = grafo.Vertices[minimo].Vecinos;

        for (auto& i : vecinos) {
            if (Tabla[i.VerticeAdyacente].Visitado == false && Tabla[minimo].Distancia + haversine(lat1, lon1, grafo.Vertices[i].Latitud, grafo.Vertices[i].Longitud) / 33 < Tabla[i.VerticeAdyacente].Distancia) {
                Tabla[i.VerticeAdyacente].Distancia = Tabla[minimo].Distancia + haversine(lat1, lon1, grafo.Vertices[i].Latitud, grafo.Vertices[i].Longitud) / 33;
                Tabla[i.VerticeAdyacente].Anterior = minimo;
            }
        }
    }
}
double AEstrella::deg2rad(double deg) {

    double pi = 2 * acos(0.0);

    return deg * (pi / 180);
}

double AEstrella::haversine(double lat1, double lon1, double lat2, double lon2) {

    float dLat = deg2rad(lat2 - lat1);
    float dLon = deg2rad(lon2 - lon1);
    float a = sin(dLat / 2) * sin(dLat / 2) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon / 2) * sin(dLon / 2);
    return 12742 * atan2(sqrt(a), sqrt(1 - a)); // Di�metro de la Tierra x todo lo dem�s :v
}


int AEstrella::EncontrarMinimioSinVisitar() {
    int distanciaMinima = TablaGrafos::ValorDistanciaInfinita();
    int indice;

    for (int i = 0; i < Tabla.size(); i++) {
        if (Tabla[i].Visitado == false && Tabla[i].Distancia < distanciaMinima) {
            distanciaMinima = Tabla[i].Distancia;
            indice = i;
        }
    }
    return indice;
}

string AEstrella::RutaMasCortaA(Grafo& grafo, int destino) {
    string res;

    while (Tabla[destino].Distancia != 0) {
        res = "->" + grafo.Vertices[destino].Nombre + res;
        destino = Tabla[destino].Anterior;
    }
    res = grafo.Vertices[destino].Nombre + res;
    return res;
}