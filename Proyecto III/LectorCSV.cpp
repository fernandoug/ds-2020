#include "LectorCSV.h"
#include <sstream>
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

LectorCSV::LectorCSV(std::string archivo, std::string delimitador)
{
	Archivo = archivo;
	Delimitador = delimitador;
}

std::vector<std::vector<std::string>> LectorCSV::getData()
{
    std::ifstream archivo(Archivo);
    std::vector<std::vector<std::string> > resultado;
    string linea = "";

    while (getline(archivo, linea)) {
        


        vector<string> nVector;
        boost::algorithm::split(nVector, linea, boost::is_any_of(Delimitador));

        resultado.push_back(nVector);

    }

    archivo.close();

    // TODO: leer archivo CSV y devolver vector de vector de strings
    // lean linear por linea y agreguen al vector resultado
    // deben separar las lineas por Delimitador para obtener las columnas
    return resultado;
}
