#pragma once
#include "TablaGrafos.h"
#include "Grafo.h"
#include <stack>
#include <vector>
#include <sstream>


class AEstrella
{
public:

	std::vector<TablaGrafos> Tabla;

	
	void Inicializar(Grafo& grafo, int fuente);

	
	void Ejecutar(Grafo& grafo, int fuente);

	double deg2rad(double deg);

	double haversine(double lat1, double lon1, double lat2, double lon2);

	int EncontrarMinimioSinVisitar();

	
	std::string RutaMasCortaA(Grafo& grafo, int destino);

};