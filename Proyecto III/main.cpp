#include <iostream>
#include "Grafo.h"
#include "CargarLondres.h"
#include "Dijkstra.h"
#include "Prim.h"
#include "AEstrella.h"
using namespace std;

int opcion;
Grafo londres;
string estaciones = "london.stations.csv";
string conexiones = "london.connections.csv";

CargarLondres cargador(estaciones, conexiones);
cargador.Cargar(londres);

string fuente, destino;
int intFuente, intDestino;

Dijkstra dik; 
Prim prim;
AEstrella aStar;

int estacionID = londres.EncontrarVerticePorNombre(fuente);

int main()
{
    while (true) {
        cout << "\n1. Consultar estaci�n por nombre"
            << "\n2. Ruta m�s corta (Dijkstra)"
            << "\n3. Ruta m�s corta (Prim)"
            << "\n4. Ruta m�s corta (A*)"
            << "\nOpci�n: ";
        cin >> opcion;

        switch (opcion) {
        
        case 1:
            cout << "\nEstaci�n: ";
            cin >> fuente;
            
            if (estacionID != -1) {

              Vertice estacion = londres.Vertices[estacionID];

              string vecinos;
              for (auto& v : estacion.Vecinos) {

                  vecinos += to_string(v.VerticeAdyacente) + " ";
              }
              cout << "\nNombre: " << estacion.Nombre
                  << "\nLatitud: " << estacion.Latitud
                  << "\nLongitud: " << estacion.Longitud
                  << "\nVecinos: " << vecinos;
            }
            break;

        case 2:
            cout << "\nFuente: ";
            cin >> fuente;
            cout << "\nDestino: ";
            cin >> destino;

            
            intFuente = londres.EncontrarVerticePorNombre(fuente);
            intDestino = londres.EncontrarVerticePorNombre(destino);
            dik.Ejecutar(londres, intFuente);
            cout << dik.RutaMasCortaA(londres, intDestino);
            break;

        case 3:
            cout << "\nFuente: ";
            cin >> fuente;
            cout << "\nDestino: ";
            cin >> destino;

         
            intFuente = londres.EncontrarVerticePorNombre(fuente);
            intDestino = londres.EncontrarVerticePorNombre(destino);
            prim.Ejecutar(londres, intFuente);
            cout << prim.RutaMasCortaA(londres, intDestino);
            break;

        case 4:
            cout << "\nFuente: ";
            cin >> fuente;
            cout << "\nDestino: ";
            cin >> destino;

            
            intFuente = londres.EncontrarVerticePorNombre(fuente);
            intDestino = londres.EncontrarVerticePorNombre(destino);
            aStar.Ejecutar(londres, intFuente);
            cout << aStar.RutaMasCortaA(londres, intDestino);
            break;
        }
    }
}